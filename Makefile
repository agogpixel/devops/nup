SCRIPT_PATH=nup.sh

DIST_DIR_PATH=dist
DIST_SCRIPT_PATH=$(DIST_DIR_PATH)/nup

UNIT_TEST_PATH=test/nup.bats
E2E_TEST_PATH=test/nup.e2e.bats
COVERAGE_PATH=coverage

CLEAN_COMMAND=rm -rf
TEST_COMMAND=docker run -v "$(CURDIR)":/mnt/workspace --rm --name cave registry.gitlab.com/agogpixel/devops/docker/cave:latest
BUILD_COMMAND=mkdir -p $(DIST_DIR_PATH) && cp $(SCRIPT_PATH) $(DIST_SCRIPT_PATH)
RELEASE_COMMAND=

.PHONY: all
all: clean unit-test build e2e-test

.PHONY: clean
clean: clean-coverage clean-dist

.PHONY: clean-coverage
clean-coverage:
	$(info [clean-coverage])
	$(CLEAN_COMMAND) $(COVERAGE_PATH)

.PHONY: clean-dist
clean-dist:
	$(info [clean-dist])
	$(CLEAN_COMMAND) $(DIST_DIR_PATH)

.PHONY: unit-test
unit-test: $(UNIT_TEST_PATH) $(SCRIPT_PATH)
	$(info [unit-test])
	$(TEST_COMMAND) $<

.PHONY: e2e-test
e2e-test: $(E2E_TEST_PATH) $(DIST_SCRIPT_PATH)
	$(info [e2e-test])
	$(TEST_COMMAND) $<

build: $(SCRIPT_PATH)
	$(info [build])
	$(BUILD_COMMAND)

RELEASE_SEMVER?=

.PHONY: release
release:
	$(info [release])
	$(RELEASE_COMMAND) $(RELEASE_SEMVER)
