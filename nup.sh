#!/bin/bash

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
#set -o xtrace

################################################################################
# Settings
################################################################################

# Error codes.
declare -A NUP_ERR_CODES=([OPT]=1 [TREE]=2 [REFS]=3 [UNTRACKED]=4 [CLEAN]=5 [STAGED]=6)

################################################################################
# Options
################################################################################

# Print help.
declare -A NUP_OPT_HELP=([NAME]=help [LONG]=help [SHORT]=h)

################################################################################
# Functions
################################################################################

nup_main() {
  nup_parse_args $@
  nup_get_short_commit_hash
}

nup_parse_args() {
  local opts="${NUP_OPT_HELP[SHORT]}"
  local longopts="${NUP_OPT_HELP[LONG]}"

  ! PARSED=$(getopt --options=$opts --longoptions=$longopts --name "$0" -- "$@")

  if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has complained about wrong arguments to stdout.
    nup_print_usage
    exit ${NUP_ERR_CODES[OPT]}
  fi

  # Handle quoting in getopt output.
  eval set -- "$PARSED"

  while true; do
    case "$1" in
      "-${NUP_OPT_HELP[SHORT]}"|"--${NUP_OPT_HELP[LONG]}")
        printf '\nDisplay the Git short commit hash for HEAD in current repository.\n\n'
        nup_print_usage
        printf '\nSee https://gitlab.com/agogpixel/devops/nup for more info.\n'
        exit 0
        ;;
      --)
        shift
        break
        ;;
    esac
  done
}

nup_get_short_commit_hash() {
  if nup_inside_git_work_tree || exit ${NUP_ERR_CODES[TREE]}; then
    if nup_ref_exists || exit ${NUP_ERR_CODES[REFS]}; then
      if nup_no_untracked_files || exit ${NUP_ERR_CODES[UNTRACKED]}; then
        if nup_is_branch_clean || exit ${NUP_ERR_CODES[CLEAN]}; then
          if nup_no_staged_changes || exit ${NUP_ERR_CODES[STAGED]}; then
            git rev-parse --short HEAD --
          fi
        fi
      fi
    fi
  fi
}

nup_print_usage() {
  cat <<EOF
Usage:
  $0

  No output if, in order of precedence:
    - Not a Git repo.  Error code: 2
    - No refs.         Error code: 3
    - Untracked files. Error code: 4
    - Modified files.  Error code: 5
    - Staged files.    Error code: 6
EOF
}

nup_inside_git_work_tree() {
  [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" = 'true' ]
}

nup_ref_exists() {
  [ "$(git show-ref | wc -l)" -gt 0 ]
}

nup_no_untracked_files() {
  [ "$(git ls-files --other --directory --exclude-standard | wc -l)" -eq 0 ]
}

nup_is_branch_clean() {
  [ "$(git diff --shortstat 2> /dev/null | tail -n1)" = "" ]
}

nup_no_staged_changes() {
  if git diff-index --quiet --cached HEAD --; then
    return 0
  fi

  return 1
}

################################################################################
# Program Entry
################################################################################

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  nup_main $@
fi

