# nup

Display the Git short commit hash for HEAD in current repository.

## Usage

```bash
nup
```

No output if, in order of precedence:

| Condition        | Error Code |
|:----------------:|:----------:|
| Not a Git repo.  | 2          |
| No refs.         | 3          |
| Untracked files. | 4          |
| Modified files.  | 5          |
| Staged files.    | 6          |

## Development

- Project utilizes [GitHub flow](https://guides.github.com/introduction/flow/).
