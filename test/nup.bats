load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

load 'test-lib'

@test "[no_staged_changes] Succes when Git repo has no staged files." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-no-staged-changes-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_no_staged_changes

  assert_success
  assert_output ''
}

@test "[no_staged_changes] Error when Git repo has staged files." {
  source ./nup.sh

  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/unit-test-no-staged-changes-error && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  git add . &> /dev/null && \
  run nup_no_staged_changes

  assert_failure
  assert_output ''
}

@test "[is_branch_clean] Success when Git repo has no modified files." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-is-branch-clean-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_is_branch_clean

  assert_success
  assert_output ''
}

@test "[is_branch_clean] Error when Git repo has modified files." {
  source ./nup.sh

  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/unit-test-is-branch-clean-error && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  run nup_is_branch_clean

  assert_failure
  assert_output ''
}

@test "[no_untracked_files] Success when Git repo has no untracked files." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-no-untracked-files-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_no_untracked_files

  assert_success
  assert_output ''
}

@test "[no_untracked_files] Error when Git repo has untracked files." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-no-untracked-files-error && \
  nup_test_git_repo_initial_commit test01.txt && \
  touch test02.txt &> /dev/null && \
  run nup_no_untracked_files

  assert_failure
  assert_output ''
}

@test "[ref_exists] Success when Git repo has refs." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-ref-exists-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_ref_exists

  assert_success
  assert_output ''
}

@test "[ref_exists] Error when Git repo has no refs" {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-ref-exists-error && \
  run nup_ref_exists

  assert_failure
  assert_output ''
}

@test "[inside_git_work_tree] Success when a Git repo" {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-inside-git-work-tree-success && \
  run nup_inside_git_work_tree

  assert_success
  assert_output ''
}

@test "[inside_git_work_tree] Error when not a Git repo" {
  source ./nup.sh

  local repo_path=/tmp/unit-test-inside-git-work-tree-error

  mkdir -p "$repo_path" && \
  cd "$repo_path" && \
  run nup_inside_git_work_tree

  assert_failure
  assert_output ''
}

@test "[print_usage] Prints usage." {
  source ./nup.sh

  run nup_print_usage

  assert_success
  assert_output --partial "Usage:"
}

@test "[get_short_commit_hash] Exits with error code 2 (not a Git repo)." {
  source ./nup.sh

  local repo_path=/tmp/unit-test-get-short-commit-hash-error-code-2

  mkdir -p "$repo_path" && \
  cd "$repo_path" && \
  run nup_get_short_commit_hash

  assert_failure 2
  assert_output ''
}

@test "[get_short_commit_hash] Exits with error code 3 (Git repo has no refs)." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-get-short-commit-hash-error-code-3 && \
  run nup_get_short_commit_hash

  assert_failure 3
  assert_output ''
}

@test "[get_short_commit_hash] Exits with error code 4 (Git repo has untracked files)." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-get-short-commit-hash-error-code-4 && \
  nup_test_git_repo_initial_commit test01.txt && \
  touch test02.txt &> /dev/null && \
  run nup_get_short_commit_hash

  assert_failure 4
  assert_output ''
}

@test "[get_short_commit_hash] Exits with error code 5 (Git repo has modified files)." {
  source ./nup.sh

  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/unit-test-get-short-commit-hash-error-code-5 && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  run nup_get_short_commit_hash

  assert_failure 5
  assert_output ''
}

@test "[get_short_commit_hash] Exits with error code 6 (Git repo has staged files)." {
  source ./nup.sh

  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/unit-test-get-short-commit-hash-error-code-6 && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  git add . &> /dev/null && \
  run nup_get_short_commit_hash

  assert_failure 6
  assert_output ''
}

@test "[get_short_commit_hash] Prints 7 character Git commit short hash for HEAD." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/e2e-get-short-commit-hash-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_get_short_commit_hash

  assert_success
  assert_output --regexp '^[0-9a-f]{7}$'
}

@test "[parse_args] Success when no arguments provided." {
  source ./nup.sh

  run nup_parse_args

  assert_success
  assert_output ''
}

@test "[parse_args] Success when non-option arguments provided." {
  source ./nup.sh

  run nup_parse_args a b c d

  assert_success
  assert_output ''
}

@test "[parse_args] Prints usage & returns with success when -h or --help" {
  source ./nup.sh

  run nup_parse_args -h

  assert_success
  assert_output --partial "Usage:"

  run nup_parse_args --help

  assert_success
  assert_output --partial "Usage:"
}

@test "[parse_args] Prints usage & returns error code 1 (invalid option)." {
  source ./nup.sh

  run nup_parse_args --fail-it

  assert_failure 1
  assert_output --partial "Usage:"
}

@test "[main] Prints usage & exits with success when -h or --help." {
  source ./nup.sh

  run nup_main -h

  assert_success
  assert_output --partial "Usage:"

  run nup_main --help

  assert_success
  assert_output --partial "Usage:"
}

@test "[main] Prints usage & exits with error code 1 (invalid option)." {
  source ./nup.sh

  run nup_main --fail-it

  assert_failure 1
  assert_output --partial "Usage:"
}

@test "[main] Prints nothing & exits with error code 2 (not a Git repo)." {
  source ./nup.sh

  local repo_path=/tmp/unit-test-main-error-code-2

  mkdir -p "$repo_path" && \
  cd "$repo_path" && \
  run nup_main

  assert_failure 2
  assert_output ''
}

@test "[main] Prints nothing & exits with error code 3 (Git repo has no refs)." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-main-error-code-3 && \
  run nup_main

  assert_failure 3
  assert_output ''
}

@test "[main] Prints nothing & exits with error code 4 (Git repo has untracked files)." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/unit-test-main-error-code-4 && \
  nup_test_git_repo_initial_commit test01.txt && \
  touch test02.txt &> /dev/null && \
  run nup_main

  assert_failure 4
  assert_output ''
}

@test "[main] Prints nothing & exits with error code 5 (Git repo has modified files)." {
  source ./nup.sh

  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/unit-test-main-error-code-5 && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  run nup_main

  assert_failure 5
  assert_output ''
}

@test "[main] Prints nothing & exits with error code 6 (Git repo has staged files)." {
  source ./nup.sh

  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/unit-test-main-error-code-6 && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  git add . &> /dev/null && \
  run nup_main

  assert_failure 6
  assert_output ''
}

@test "[main] Prints 7 character Git commit short hash for HEAD." {
  source ./nup.sh

  nup_test_git_repo_create /tmp/e2e-test-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_main

  assert_success
  assert_output --regexp '^[0-9a-f]{7}$'
}
