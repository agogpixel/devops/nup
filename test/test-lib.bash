nup_test_git_repo_create() {
  local repo_path="$1"

  mkdir -p "$repo_path" && \
  cd "$repo_path" && \
  git init --quiet && \
  git config user.email "test@test.com" && \
  git config user.name "Test"
}

nup_test_git_repo_initial_commit() {
  local filename="$1"

  touch "$filename" &> /dev/null && \
  git add . &> /dev/null && \
  git commit -m "Test." --quiet
}

nup_test_git_repo_modify_file() {
  local modified_filename="$1"

  set +o noclobber && \
  echo 'Test' > "$modified_filename" && \
  set -o noclobber
}
