load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

load 'test-lib'

readonly DIST_SCRIPT_PATH=/mnt/workspace/dist/nup

nup_e2e_exit_if_not_found() {
  if [ ! -f "$DIST_SCRIPT_PATH" ]; then
    printf '%s not found' "$DIST_SCRIPT_PATH"
    exit 1
  fi
}

nup_e2e_dist() {
  /bin/bash "$DIST_SCRIPT_PATH" $@
  return $?
}

nup_e2e_exit_if_not_found

@test "Prints usage & exits with success when -h or --help." {
  run nup_e2e_dist -h

  assert_success
  assert_output --partial "Usage:"

  run nup_e2e_dist --help

  assert_success
  assert_output --partial "Usage:"
}

@test "Prints usage & exits with error code 1 (invalid option)." {
  run nup_e2e_dist --fail-it

  assert_failure 1
  assert_output --partial "Usage:"
}

@test "Prints nothing & exits with error code 2 (not a Git repo)." {
  local repo_path=/tmp/e2e-test-error-code-2

  mkdir -p "$repo_path" && \
  cd "$repo_path" && \
  run nup_e2e_dist

  assert_failure 2
  assert_output ''
}

@test "Prints nothing & exits with error code 3 (Git repo has no refs)." {
  nup_test_git_repo_create /tmp/e2e-test-error-code-3 && \
  run nup_e2e_dist

  assert_failure 3
  assert_output ''
}

@test "Prints nothing & exits with error code 4 (Git repo has untracked files)." {
  nup_test_git_repo_create /tmp/e2e-test-error-code-4 && \
  nup_test_git_repo_initial_commit test01.txt && \
  touch test02.txt &> /dev/null && \
  run nup_e2e_dist

  assert_failure 4
  assert_output ''
}

@test "Prints nothing & exits with error code 5 (Git repo has modified files)." {
  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/e2e-test-error-code-5 && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  run nup_e2e_dist

  assert_failure 5
  assert_output ''
}

@test "Prints nothing & exits with error code 6 (Git repo has staged files)." {
  local modified_filename=test01.txt

  nup_test_git_repo_create /tmp/e2e-test-error-code-6 && \
  nup_test_git_repo_initial_commit "$modified_filename" && \
  nup_test_git_repo_modify_file "$modified_filename" && \
  git add . &> /dev/null && \
  run nup_e2e_dist

  assert_failure 6
  assert_output ''
}

@test "Prints 7 character Git commit short hash for HEAD." {
  nup_test_git_repo_create /tmp/e2e-test-success && \
  nup_test_git_repo_initial_commit test01.txt && \
  run nup_e2e_dist

  assert_success
  assert_output --regexp '^[0-9a-f]{7}$'
}
